# -*- coding: utf-8 -*-
"""
Created on Fri Nov 07 21:21:01 2014

@author: shishir
@email: shishir.py@gmail.com

Mining python documentation to find patterns for associate dynamic data types.
"""

import ast
import os
import re
from sphinxcontrib.napoleon import NumpyDocstring

class DocumentationCollector(ast.NodeVisitor):
    """
    Get the documentation of each function and 
    """
    
    def __init__(self, filename):
        """
        Both *_docs are the form
        {file.fn: doc}
        """
        self.class_docs = dict()
        self.func_docs = dict()
        self.filename = filename
        self.all_docs = list()
        
    def visit_FunctionDef(self, node):
        name = node.name
        key = self.filename + '-' + name
        doc_string = ast.get_docstring(node)
        self.all_docs.append(doc_string)
        
        
    
    def visit_ClassDef(self, node):
        name = node.name
        key = self.filename + '-' + name
        doc_string = ast.get_docstring(node)
        self.all_docs.append(doc_string)
        
        
    def generic_visit(self, node):
        super(DocumentationCollector, self).generic_visit(node)
        
        
if __name__ == "__main__":
    # run it on many files    
    path = "../../datasets/pythonfiles/"
    
    # will contain list of lists of all targets in all files
    docstrings = list() 
    
    # Go through all files in the folder
    for root, subfolders, files in os.walk(path):
        for f in files:
            m = re.findall('\.py$', f)
            calls = list()
            aliases = list()
            if m:
                code = ""
                python_file = open(root + "/" + f)
                for l in python_file:
                    code += l
                # print python_file
                # print '\n'
                tree = ast.parse(code)
                
                dc = DocumentationCollector('hello')
                dc.visit(tree)
                
                for i in dc.all_docs:
                    if i is not None:
                        docstrings.append(i)
                        
    f = open('../../datasets/pythonfiles/docstrings.txt', 'w')    
    for docstr in docstrings:
        f.write(docstr)
        f.write('\n\n')
        f.write("*" * 50)
        f.write('\n\n')
    f.close()
        
    f = open('../../datasets/pythonfiles/reStructureDoc.txt', 'w')    
    for docstr in docstrings:
        s = NumpyDocstring(docstr)
        for line in s.lines():
            f.write(line)
            f.write('\n')
        f.write('\n\n')
        f.write('*' * 50)
        f.write('\n\n')
    f.close()
        
            