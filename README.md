# README #

This project is to study the documentation string of various known libraries and to see if one can associate data types to variables based on the docstring of the function or the class.

### What is this repository for? ###

* To understand the doc string and data type association.
* Version 0.1


### Type of Docstrings ###
I know of two standards which people generally follow to write docstrings. One is the NumPy style docstring and the other is Google style docstring. Example for the NumPy style docstring can be found [here](http://bit.ly/1wNsApk). If you have used any of the scientific computing packages in python such as NumPy, SciPy, scikit-learn, Networkx, matplotlib, etc. they all use the NumPy style docstring. The NumPy style docstring look something like the following (copied from [sphinxcontrib](http://sphinxcontrib-napoleon.readthedocs.org/en/latest/index.html#google-vs-numpy)):


```
#!python

def func(arg1, arg2):
    """Summary line.

    Extended description of function.

    Parameters
    ----------
    arg1 : int
        Description of arg1
    arg2 : str
        Description of arg2

    Returns
    -------
    bool
        Description of return value

    """
    return True
```

An example of the Google style python docstrings can be found [here](http://bit.ly/1uYrAzS). The Google style python docstrings look like the following (copied from [sphinxcontrib](http://sphinxcontrib-napoleon.readthedocs.org/en/latest/index.html#google-vs-numpy)):

```
#!python

def func(arg1, arg2):
    """Summary line.

    Extended description of function.

    Args:
        arg1 (int): Description of arg1
        arg2 (str): Description of arg2

    Returns:
        bool: Description of return value

    """
    return True
```


Python is a dynamic programming language and it is tough to find the type of an object without running it. But if the author of the program have written the doc string a standard format it will be easier to perform static analysis of the code.
